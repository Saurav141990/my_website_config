﻿#Folder Module
C:\config\appconfiguration\modules\folder\files\folder.ps1

#File Creation Path
#---------------------------------

$file =  "C:\custom-logs\" + "install-start_" + (Get-Date).ToString("MM-dd-yy-hh-mm-ss")
$datetime = (Get-Date).ToString("MM-dd-yy-hh-mm-ss")
New-Item $file -ItemType file

Write-Output "--$datetime--Install-start start"
"--$datetime--Install-start start" | Out-File $file -Append


Write-Output "--$datetime--folder module completed"
"--$datetime--folder module completed" | Out-File $file -Append

#download_mywebsite code Module
C:\config\appconfiguration\modules\download_mywebsite_code\files\download_mywebsite_code.ps1
Write-Output "--$datetime--download_software module completed"
"--$datetime--download_software module completed" | Out-File $file -Append

#Notepad++ Module
#C:\config\appconfiguration\modules\notepadplusplus\files\notepadplusplus.ps1
#Write-Output "--$datetime--notepadplusplus completed"
#"--$datetime--notepadplusplus completed" | Out-File $file -Append


C:\config\appconfiguration\modules\web-bindings\files\web-bindings.ps1


Write-Output "--$datetime--Install-start End"
"--$datetime--Install-start End" | Out-File $file -Append