# Title : Installing Windows Feature - IIS (WebServer) using PowerShell
#                                                         -By Saurav Sekhar
#Get-Command *feature*
#Get-WindowsFeature *Web*server*
$IsInstalled = ((Get-WindowsFeature -Name Web-Server).installed)
if ($isinstalled -eq $false){
    Install-WindowsFeature Web-Server -IncludeManagementTools -IncludeAllSubFeature -Confirm:$false
    #Write-Output "Installation Complete"
	#"--$datetime--Installation Complete Ready to restart" | Out-File $file -Append
	#Restart-Computer
}
else{
    Write-output "IIS Server (WebServer) is already installed"
	#"--$datetime--IIS Server (WebServer) is already installed" | Out-File $file -Append
	
}
