#https://www.youtube.com/watch?v=iVO60E8uvNc
Import-Module webAdministration
Get-Website
#md c:\AppCode
New-Item -Path "IIS:\Sites" -Name "mywebsite" -Type Site -Bindings @{protocol="HTTP";BindingInformation="*:80:"} -physicalPath "C:\AppCode\mywebsite"
Set-ItemProperty -Path "IIS:\Sites\mywebsite" -Name "physicalPath" -Value "C:\AppCode\mywebsite"

Set-ItemProperty -Path "IIS:\Sites\Default Web Site" -Name "bindings" -Value @{protocol="HTTP";BindingInformation="*:81:"}

Get-Website |select *
#Get-Website website1 |Remove-Website
Get-Website mywebsite | Start-Website
Get-Website -Name ""
$logsetup = @{logFormat="W3c";
 enabled =$true
  directory ="C:\AppCode\iislogs"
  period = "Daily"
  }
  Set-ItemProperty -Path "IIS:\Sites\mywebsite" -Name "logFile" -Value $logsetup
  Get-ItemProperty -Path "IIS:\Sites\mywebsite" -Name "logFile" 
  
 